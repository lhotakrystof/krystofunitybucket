﻿using UnityEngine;
using UnityEngine.Rendering;
public class DragAndDrop : MonoBehaviour
{
public GameObject SelectedPiece;
int LL = 1;
    void Start()
    {
        
    }

    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hit = Physics2D.Raycast( origin: Camera.main.ScreenToWorldPoint(Input.mousePosition), direction: Vector2.zero);
            if (hit.transform.CompareTag("Piece"))
            {   
                if (!hit.transform.GetComponent<PiecesScript>().InRightPosition)
                {
                SelectedPiece = hit.transform.gameObject;
                SelectedPiece.GetComponent<PiecesScript>().Selected = true;
                SelectedPiece.GetComponent<SortingGroup>().sortingOrder = LL;
                LL++;
                }
            }
        }
        
        if (Input.GetMouseButtonUp(0))
        {
            SelectedPiece.GetComponent<PiecesScript>().Selected = false;
            SelectedPiece = null;
        }

        if(SelectedPiece != null)
        {
            Vector3 MousePoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            SelectedPiece.transform.position = new Vector3(MousePoint.x, MousePoint.y, z: 0);
        }
    }
}

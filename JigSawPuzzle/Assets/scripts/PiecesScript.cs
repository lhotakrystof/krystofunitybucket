﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class PiecesScript : MonoBehaviour
{
    // Start is called before the first frame update
    public Vector3 RightPosition;
    public bool InRightPosition;
    public bool Selected;
    public Transform edgeParticle;
    public Transform youWin;
    static public int counter = 0;
    public AudioClip clip;
    public AudioSource source;
    public Text scoreText;
    //public Text CurrentScore;
    public Vector3 TextPosition;

    void Start()
    {
        RightPosition = transform.position;
        transform.position = new Vector3(x: Random.Range(8f, 15f), y: Random.Range(5f,-5.5f));
    }

    // Update is called once per frame
    void Update()
    {
        if(Vector3.Distance( a: transform.position, b: RightPosition) < 0.5f)
        {
            if(!Selected){
                if(InRightPosition == false)
                {
                transform.position = RightPosition;
                InRightPosition = true;

                //Instantiate(edgeParticle, gameObject.transform.position, edgeParticle.rotation);
                GetComponent<SortingGroup>().sortingOrder = 0;
                counter++;
                if (counter != 36)
                {
                        if (counter >= 10)
                        {
                          TextPosition = scoreText.transform.position;
                          scoreText.transform.position = new Vector2(2540, 1285);
                        }
                    FindObjectOfType<PlaySound>().PlayRightPositionSound();
                }
                if (counter == 36)
                {
                        FindObjectOfType<PlaySound>().PlayWinSound();
                }
                }
            }
        }
                scoreText.text = counter.ToString("0");
    }
}

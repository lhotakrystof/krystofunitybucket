﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySound : MonoBehaviour
{   
    public AudioClip rightPosition;
    public AudioClip winSound;
    public AudioClip youDidIt;
    public AudioClip StartSound;
    public AudioSource source;

    // Start is called before the first frame update

    // Update is called once per frame
    public void StartGame(){Invoke("Beginning", 1);}
    public void Beginning(){source.PlayOneShot(StartSound);}
    public void PlayRightPositionSound(){source.PlayOneShot(rightPosition);}
    public void PlayWinSound(){
        source.PlayOneShot(winSound);
        Invoke("PlayYouDidIt", 2.7f);}
    public void PlayYouDidIt(){source.PlayOneShot(youDidIt);}
    
}
